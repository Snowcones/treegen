#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "mesh.hpp"

// Code to render generated trees
class Renderer {
 public:
  void render_mesh(const Mesh& mesh);
};

#endif /* RENDERER_HPP */
