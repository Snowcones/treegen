#include <iostream>
#include "mesh.hpp"
#include "renderer.hpp"
#include "tree_planter.hpp"

int main() {
  Tree tree = TreePlanter::DefaultTree();
  Renderer renderer;
  for (int i = 0; i < 30; i++) {
    std::cout << "Grown for " << i << " years." << std::endl;
    tree.grow_year();
    Mesh tree_mesh = Mesh::from_tree(tree);
    renderer.render_mesh(tree_mesh);
  }
}
