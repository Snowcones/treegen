#ifndef TREEGEN_HPP
#define TREEGEN_HPP

// Generates a mesh from a tree structure

#include <vector>
#include "glm/vec3.hpp"
#include "tree.hpp"

struct Triangle {
  glm::vec3 t1;
  glm::vec3 t2;
  glm::vec3 t3;
};

class Mesh {
  std::vector<Triangle> triangles;

 public:
  static Mesh from_tree(const Tree& tree);
};

#endif /* TREEGEN_HPP */
