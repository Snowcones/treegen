#ifndef APICAL_CONTROL_HPP
#define APICAL_CONTROL_HPP

class ApicalControl {
 public:
  virtual double branch_bias(double age) = 0;
};

class ConstantApicalControl : public ApicalControl {
  const double kBranchBias;

 public:
  ConstantApicalControl(double branch_bias) : kBranchBias(branch_bias){};
  double branch_bias(double age) override { return kBranchBias; }
};

#endif /* APICAL_CONTROL_HPP */
