#include "phyllotaxis.hpp"
#include "glm/glm.hpp"
#include "glm/gtx/projection.hpp"
#include "glm/gtx/rotate_vector.hpp"
#include "glm/gtx/vector_angle.hpp"
#include "vector_math.hpp"

glm::vec3 Phyllotaxis::next_coaxis(glm::vec3 trunk_axis, glm::vec3 coaxis) {
  return glm::rotate(coaxis, (float)kRotationAngle, trunk_axis);
}

glm::vec3 Phyllotaxis::branch(glm::vec3 trunk_axis, glm::vec3 coaxis) {
  glm::vec3 normal = glm::cross(trunk_axis, coaxis);
  glm::vec3 branch = glm::rotate(trunk_axis, (float)kBranchAngle, normal);
  // Prevent divide by zero when branch ends up vertical
  if (branch.x == 0 && branch.y == 0) {
    branch.x += .001;
  }
  glm::vec3 horizontal = glm::normalize(glm::vec3(branch.x, branch.y, 0));
  return glm::slerp(branch, horizontal, (float)kRotateToHorizontal);
}

glm::vec3 Phyllotaxis::compute_coaxis(glm::vec3 trunk_axis,
                                      glm::vec3 branch_axis) {
  return rotate_to_perpendicular(trunk_axis, branch_axis);
}
