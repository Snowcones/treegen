#ifndef LIGHT_HPP
#define LIGHT_HPP

#include "glm/vec3.hpp"

class Light {
 public:
  double get_brightness(glm::vec3 position);
  glm::vec3 gradient(glm::vec3 position);
};

#endif /* LIGHT_HPP */
