#ifndef VECTOR_MATH_HPP
#define VECTOR_MATH_HPP

#include "glm/glm.hpp"
#include "glm/gtx/projection.hpp"
#include "glm/vec3.hpp"

inline glm::vec3 rotate_to_perpendicular(glm::vec3 v, glm::vec3 normal) {
  glm::vec3 projection = glm::proj(v, normal);
  glm::vec3 rejection = v - projection;
  return glm::normalize(rejection);
}

#endif /* VECTOR_MATH_HPP */
