#include "tree.hpp"
#include "vector_math.hpp"

double Tree::get_tree_exposure(Node* node) {
  if (!node) {
    return 0;
  }
  return node->cumulative_exposure;
}

double Tree::calculate_tree_exposure(Node* node) {
  if (!node) return 0;
  double total_exposure = 0;
  total_exposure += calculate_tree_exposure(node->next_node);
  total_exposure += calculate_tree_exposure(node->branched_node);
  total_exposure += calculate_bud_exposure(node);
  node->cumulative_exposure = total_exposure;
  return total_exposure;
}

double Tree::calculate_bud_exposure(Node* node) {
  if (node && node->has_bud()) {
    return light->get_brightness(node->position);
  }
  return 0;
}

void Tree::reorient_bud(Bud* bud, glm::vec3 position) {
  assert(bud);
  glm::vec3 current_direction = bud->branch_axis;
  glm::vec3 phototropic_direction = light->gradient(position);
  glm::vec3 gravitropic_direction = glm::vec3(0, 0, 1);
  bud->branch_axis = current_direction * (float)(1 - species->kPhototropism -
                                                 species->kGravitropism) +
                     phototropic_direction * (float)species->kPhototropism +
                     gravitropic_direction * (float)species->kGravitropism;
  bud->rotation_coaxis =
      rotate_to_perpendicular(bud->rotation_coaxis, bud->branch_axis);
}

Node* Tree::grow_shoot(Node* node, Bud* bud) {
  assert(node && bud);
  reorient_bud(bud, node->position);
  node->bud = nullptr;
  Node* shoot =
      new Node(bud->branch_axis, node->position + bud->branch_axis, bud);
  bud->rotation_coaxis =
      species->phyllotaxis.next_coaxis(bud->branch_axis, bud->rotation_coaxis);
  return shoot;
}

void Tree::grow_axial_bud(Node* node, int metamers) {
  assert(node);
  assert(node->bud_is_axial());
  Bud* bud = node->bud;
  Node* shoot = grow_shoot(node, bud);
  bud->is_axial = false;
  node->branched_node = shoot;
  grow_terminal_bud(node->next_node, metamers - 1);
}

void Tree::grow_terminal_bud(Node* node, int metamers) {
  assert(node);
  assert(node->bud_is_terminal());
  Bud* bud = node->bud;
  while (metamers > 0) {
    metamers--;

    glm::vec3 branch_axis =
        species->phyllotaxis.branch(bud->branch_axis, bud->rotation_coaxis);
    glm::vec3 coaxis = species->phyllotaxis.compute_coaxis(
        node->bud->branch_axis, branch_axis);
    Bud* axial_bud = new Bud(branch_axis, coaxis, true);

    Node* shoot = grow_shoot(node, bud);
    node->next_node = shoot;
    node->bud = axial_bud;
  }
}

void Tree::grow_bud(Node* node, double food) {
  assert(food < .01 || (node && node->has_bud()));

  int metamers = (int)food;
  if (food == 0) return;

  if (node->bud_is_terminal()) {
    grow_terminal_bud(node, metamers);
  } else {
    grow_axial_bud(node, metamers);
  }
}

namespace {
double divide_resources(double trunk_exposure, double branch_exposure,
                        double food, double trunk_bias) {
  double trunk_proportion =
      trunk_bias * trunk_exposure /
      (trunk_bias * trunk_exposure + (1 - trunk_bias) * branch_exposure);
  return food * trunk_proportion;
}
}  // namespace

void Tree::grow_tree(Node* node, double food) {
  if (!node) {
    assert(food < .001);
    return;
  }

  double trunk_exposure = get_tree_exposure(node->next_node);
  double branch_exposure = get_tree_exposure(node->branched_node);
  double bud_exposure =
      node->cumulative_exposure - trunk_exposure - branch_exposure;
  double trunk_bias = 1 - species->apical_control->branch_bias(age);

  double bud_food = 0;
  double trunk_food = 0;
  double branch_food = 0;

  if (node->has_bud()) {
    if (node->bud_is_terminal()) {
      bud_food =
          divide_resources(bud_exposure, branch_exposure, food, trunk_bias);
      branch_food = food - bud_food;
    } else {
      trunk_food =
          divide_resources(trunk_exposure, bud_exposure, food, trunk_bias);
      bud_food = food - trunk_food;
    }
  } else {
    trunk_food =
        divide_resources(trunk_exposure, branch_exposure, food, trunk_bias);
    branch_food = food - trunk_food;
  }

  grow_bud(node, bud_food);
  grow_tree(node->next_node, trunk_food);
  grow_tree(node->branched_node, branch_food);
}

void Tree::grow_year() {
  calculate_tree_exposure(base);
  grow_tree(base, base->cumulative_exposure);
}

void Tree::grow_years(double years) {
  // For each season:
  //   //  Leaf to base calculate exposures
  //   //  Base to leaf growth with stored exposures and apical control

  int years_to_grow = (int)age + years - (int)age;
  age += years;
  for (int year = 0; year < years_to_grow; ++year) {
    grow_year();
  }
}
