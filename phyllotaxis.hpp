#ifndef PHYLLOTAXIS_HPP
#define PHYLLOTAXIS_HPP

#include "glm/vec3.hpp"
#include "math.h"

class Phyllotaxis {
  // The rotation angle, in radians, around the stem between neighboring buds.
  const double kRotationAngle;
  // The angle, in radians, that a bud diverges from the main branch.
  const double kBranchAngle;
  // A decimal with 0.0 meaning buds don't rotate at all, and 1.0 meaning buds
  // rotate to horizontal plane.
  const double kRotateToHorizontal;

 public:
  Phyllotaxis(double rotation_angle, double branch_angle,
              double rotate_to_horizontal)
      : kRotationAngle(rotation_angle),
        kBranchAngle(branch_angle),
        kRotateToHorizontal(rotate_to_horizontal) {}

  static Phyllotaxis default_phyllotaxis() {
    return Phyllotaxis(/*rotation_angle=*/2 * M_PI * .4,
                       /*branch_angle=*/M_PI / 2,
                       /*rotate_to_horizontal=*/0.0);
  };

  glm::vec3 next_coaxis(glm::vec3 trunk_axis, glm::vec3 coaxis);
  glm::vec3 branch(glm::vec3 trunk_axis, glm::vec3 coaxis);
  glm::vec3 compute_coaxis(glm::vec3 trunk_axis, glm::vec3 branch_axis);
};

#endif /* PHYLLOTAXIS_HPP */
