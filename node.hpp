#ifndef NODE_HPP
#define NODE_HPP

// Needs to support
//   Calculate bud exposure
//   Update bud fates
//   Add shoots
//   Shed branches
//   Update branch width

#include "glm/vec3.hpp"

class Bud {
  friend class Tree;
  friend class Node;
  glm::vec3 branch_axis;  // Possibly unneccessary in the way position is
  glm::vec3 rotation_coaxis;
  // double years_since_growth; // Very unsure where this would come in
  bool is_axial;
  // int order; Not sure if this would make things simpler
  Bud(glm::vec3 branch_axis, glm::vec3 rotation_coaxis, bool is_axial)
      : branch_axis(branch_axis),
        rotation_coaxis(rotation_coaxis),
        is_axial(is_axial){};
};

// Holds shape of a tree. Note that next_node and terminal_bud cannot both be
// non-null and branched_node and branched_bud also cannot both be non-null.
class Node {
  friend class Tree;
  glm::vec3 direction;
  glm::vec3 position;  // Could be calculated on the fly to save memory
  Node* next_node;
  Node* branched_node;
  double shed_branch_size;
  Bud* bud;                    // Could be a terminal or axial bud
  double cumulative_exposure;  // Used to implement hormonal resource allocation

 public:
  bool has_bud() { return bud != nullptr; };
  // Assumes bud is present
  bool bud_is_axial() { return bud->is_axial; };
  bool bud_is_terminal() { return !bud_is_axial(); };
  Node(glm::vec3 direction, glm::vec3 position, Bud* bud)
      : direction(direction),
        position(position),
        next_node(nullptr),
        branched_node(nullptr),
        shed_branch_size(0),
        bud(bud),
        cumulative_exposure(0){};
};

#endif /* NODE_HPP */
