CPP = g++

#CFLAGS = -Wall -Werror -pedantic -std=c++14
CFLAGS = -std=c++14

SRCS = $(wildcard *.cpp)

OBJS = $(SRCS:.cpp=.o)

all: demo

demo: $(OBJS)
	$(CPP) $(CFLAGS) -o demo $(OBJS)

.cpp.o:
	$(CPP) $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o
	rm -f demo
