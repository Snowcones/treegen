#ifndef SPECIES_HPP
#define SPECIES_HPP

// A struct that holds the info on the breed of a tree
//
// Important details of a tree:
// Proleptic or Sylleptic
//   One or the other
// Phyllotaxis
//   Rotation rate
//   Horizontal bias
// Apical control constant
//   L in [0, 1] with L > 0.5 biased towards main branch
//   and L < 0.5 biased towards lateral branch
//   L is some function of time
// Branch diameter constants
//   d^n = d1^n + d2^n
//   some exponent n usually in [2,3]
// Branch shedding constants
//   Resources / Internode cutoff
// Shoot growth direction constants
//   Phototropism, e, positive
//   Gravitropism, n, positive or negative
//   Possibly vary tropism with branch order and/or time
// Leaf info? (Possibly a rendering detail)
//   Size, shape, and color of leaves and flowers?
// Bark info?
//   Color and texture of the branches?
//

#include "apical_control.hpp"
#include "phyllotaxis.hpp"

#include <memory>

class Species {
  friend class Tree;
  // const bool kIsProleptic; // Still need to implement
  Phyllotaxis phyllotaxis;
  std::unique_ptr<ApicalControl> apical_control;
  const double kBranchDiameterExponent;
  const double kBranchShedConstant;
  const double kPhototropism;
  const double kGravitropism;

  Species(Phyllotaxis phyllotaxis,
          std::unique_ptr<ApicalControl> apical_control,
          double branch_diameter_exponent, double branch_shed_constant,
          double phototropism, double gravitropism)
      : phyllotaxis(phyllotaxis),
        apical_control(std::move(apical_control)),
        kBranchDiameterExponent(branch_diameter_exponent),
        kBranchShedConstant(branch_shed_constant),
        kPhototropism(phototropism),
        kGravitropism(gravitropism){};
};

#endif /* SPECIES_HPP */
