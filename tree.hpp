#ifndef TREE_HPP
#define TREE_HPP

#include "light.hpp"
#include "node.hpp"
#include "species.hpp"

// Code to procedurally generate trees
//
// Add growth yearly but track age as a decimal to implement leaf color change

class Tree {
  Species* species;
  Node* base;
  Light* light;
  double age;

  double get_tree_exposure(Node* node);
  double calculate_tree_exposure(Node* node);
  double calculate_bud_exposure(Node* node);
  // Allocates a new node, rotates the bud, and puts the bud at the head of the
  // new node.
  Node* grow_shoot(Node* node, Bud* bud);
  void reorient_bud(Bud* bud, glm::vec3 position);
  void grow_axial_bud(Node* node, int food);
  void grow_terminal_bud(Node* node, int food);
  void grow_bud(Node* node, double food);
  void grow_tree(Node* node, double food);

 public:
  void grow_year();
  void grow_years(double years);
};

#endif /* TREE_HPP */
